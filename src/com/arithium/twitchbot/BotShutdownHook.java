package com.arithium.twitchbot;

import java.util.Map.Entry;

import com.arithium.twitchbot.chat.TwitchChat;

public class BotShutdownHook extends Thread {
	
	@Override
	public void run() {
		for (Entry<String, TwitchChat> chat : ChannelContainer.getChannels().entrySet()) {
			TwitchChat c = chat.getValue();
			c.saveChatSettings(c.getChannels()[0]);
			c.getUsers().save(c.getChannels()[0]);
		}
		ChannelContainer.save();
		System.out.println("Successfully saved all channels.");
	}

}
