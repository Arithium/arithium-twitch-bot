package com.arithium.twitchbot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.chat.TwitchGroup;
import com.arithium.twitchbot.point.TwitchUser;

/**
 * The chat engine for the bot client which runs on a 1 second delay.
 * 
 * @author Arithium
 *
 */
public class ChatEngine implements Runnable {

	/**
	 * A single threaded executor service to run at a 1 second delay
	 */
	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

	/**
	 * Constructs a new {@link ChatEngine} to run at a 1 second delay
	 */
	public ChatEngine() {
		executor.scheduleAtFixedRate(this, 1, 1, TimeUnit.SECONDS);
	}
	
	public static boolean shutdown;

	@Override
	public void run() {
		try {
			
			if (shutdown) {
				System.exit(0);
				return;
			}

			/*
			 * The frame or the chat is null, don't process anymore as we aren't
			 * logged in
			 */
			if (BotBootstrap.getBoostrap().getFrame() == null || BotBootstrap.getBoostrap().getChat() == null || BotBootstrap.getBoostrap().getChat().getChannels().length < 1) {
				return;
			}

			/*
			 * Loop through all of the channels and update them
			 */
			synchronized (ChannelContainer.getChannels()) {
				for (Entry<String, TwitchChat> entry : ChannelContainer.getChannels().entrySet()) {

					/*
					 * Create our chat instance
					 */
					TwitchChat chat = entry.getValue();

					/*
					 * Tick our chats refresh delay since we aren't updating
					 * every second
					 */
					chat.tickRefreshDelay();

					/*
					 * Every 120 seconds, refresh the frame and award points to
					 * the chatters
					 */
					if (chat.getRefreshDelay() % 60 == 0) {

						/*
						 * Fetch the latest twitch group for the channel
						 */
						TwitchGroup twitchGroup = BotFunctions.fetchLatestGroup(chat);

						/*
						 * Something went wrong and couldn't find the channel
						 * group, skip to the next one
						 */
						if (twitchGroup == null) {
							continue;
						}

						/*
						 * Create a new list to add the users
						 */
						List<String> users = new ArrayList<>();

						/*
						 * Add the moderators to the list
						 */
						for (String s : twitchGroup.getChatters().getModerators()) {
							users.add(s);
						}

						/*
						 * Add the viewers to the list
						 */
						if (users.size() > 0) {
							for (String s : twitchGroup.getChatters().getViewers()) {
								users.add(s);
							}
						}

						/*
						 * Loop through all the users and increment their time
						 * by 1 minute, don't award points if the streamer isn't
						 * in chat
						 */
						if (users.contains(chat.getChannels()[0].replaceAll("#", ""))) {
							for (String s : users) {
								TwitchUser point = chat.getUsers().get(s);
								if (point != null)
									point.setMinutes(point.getMinutes() + 1);
							}

							/*
							 * Add 5 points every 2 minutes
							 */
							if (chat.getRefreshDelay() % 120 == 0 && users.size() > 0) {
								try {
									for (String s : users) {
										chat.getUsers().get(s).addPoints(5);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}

								/*
								 * Reset after 120 seconds
								 */
								chat.resetRefreshDelay();
							}
						}

						/*
						 * Save the channels users
						 */
						chat.getUsers().save(chat.getChannels()[0]);

					}

					/*
					 * Cycle the channels quest handler
					 */
					chat.getQuest().questCycle();
					
					try {
					chat.getBattleArena().process();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
