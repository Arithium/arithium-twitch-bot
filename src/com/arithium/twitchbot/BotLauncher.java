package com.arithium.twitchbot;

import java.io.IOException;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.json.JSONException;

/**
 * The launcher for the bot client
 * 
 * @author arithium
 *
 */
public class BotLauncher {

	/**
	 * The main method of the bot client which launches the bot
	 * 
	 * @param args
	 * @throws IOException
	 * @throws JSONException
	 * @throws NickAlreadyInUseException
	 * @throws IrcException
	 */
	public static void main(String[] args) throws IOException, JSONException, NickAlreadyInUseException, IrcException {
		BotBootstrap boostrap = new BotBootstrap();
		boostrap.build();
		Runtime.getRuntime().addShutdownHook(new BotShutdownHook());
	}

}
