package com.arithium.twitchbot.battle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.point.TwitchUser;

public class BattleArena {

	private TwitchChat chat;

	public BattleArena(TwitchChat chat) {
		this.chat = chat;
	}

	private List<TwitchUser> users = new ArrayList<>();

	private boolean active;

	private int waitTimer;

	private int battleTimer;
	
	private long lastBattleTime;

	public void addUser(TwitchUser user, int points) {
		if (users.stream().anyMatch(i -> i.getUsername().equalsIgnoreCase(user.getUsername()))) {
			return;
		}
		
		if (active) {
			return;
		}
		
		user.removePoints(points);
		users.add(new TwitchUser(user.getUsername(), points));
		
		if (users.size() == 1) {
			writeToChat("A new battle arena instance has started! Type !battle <bet> to join now.");
		}
		
	}

	public void process() {
		if (!active) {
			if (users.size() == 0) {
				return;
			}
			
			waitTimer++;

//			System.out.println("Wait Timer: " + waitTimer);
			if (waitTimer % 60 == 0 && waitTimer < 120) {
				writeToChat("The Battle Arena starts in " + (waitTimer / 60) + " minutes. Type !battle <bet> to join now.");
			} else if (waitTimer == 90) {
				writeToChat("The Battle Arena starts in 30 seconds. Type !battle <bet> to join now.");
			} else if (waitTimer == 120) {
				if (users.size() > 1) {
					active = true;
					battleTimer = 0;
					writeToChat("The Battle Arena has started!");
				} else {
					writeToChat("Not enough players in the battle arena. Type !battle to join now.");
					waitTimer = 60;
				}
			}
		} else {
			activeCycle();
		}
	}

	private void activeCycle() {
		battleTimer++;
//		System.out.println("battle timer: " + battleTimer);
		if (battleTimer % 30 == 0) {

			if (users.size() <= 1) {
				handleVictory();
				return;
			}

			List<TwitchUser> cloned = new ArrayList<>(users);
			TwitchUser user1 = cloned.get(BotFunctions.random(cloned.size() - 1));
			cloned.remove(user1);
			TwitchUser user2 = cloned.get(BotFunctions.random(cloned.size() - 1));
			
			if (user1 == null || user2 == null || user1.getUsername().equalsIgnoreCase(user2.getUsername())) {
				return;
			}
			
			String message = getRandomAttackMessage().replace("%username", user1.getUsername()).replace("%asdf2", user2.getUsername());
			users.remove(user2);
			writeToChat(message);
		}
	}

	private final String[] RANDOM_MESSAGES = { 
			"%username stabs %asdf2 in the face with a dagger.",
			"%username slaps %asdf2 in the knee with a skillet.",
			"%asdf2 gets stabbed in the back by %username.",
			"%asdf2 gets piked in the face by %username.",
			"%asdf2 gets their toe smashed by %username.",
			"%username whips %asdf2 with a wet towel.",
			"%username distracted %asdf2 with a shiny object and knocked them out.",
			"%username flails %asdf2 with a morning star.",
			"%asdf2 turns around and gets knocked out by %username.",
			"%asdf2 gets their ear sliced by %username with a spear.",
			"%asdf2 gets shot in the hand by %username's bow.",
			"%asdf2 gets speared in the gut by %username.",
			"%asdf2 doesn't see the chair flying their way by %username and gets knocked out.",
			"%username sticks a fork up %asdf2's nose.",
			"%username throws their shield at %asdf2 knocking them off their feet.",
			"%asdf2 jumps into scuffle and gets punched in the side by %username, and unable to breathe, passes out.",
			"%username tricks %asdf2 into partnering up, only to hit them with a mop from behind.",
			"%username gives %asdf2 a wet willy.",
			"%username threw a spear across the arena and hit %asdf2.",
	};

	private String getRandomAttackMessage() {
		return RANDOM_MESSAGES[BotFunctions.random(RANDOM_MESSAGES.length - 1)];
	}

	private void handleVictory() {
		if (users.size() > 0) {
			TwitchUser winner = users.get(BotFunctions.random(users.size() - 1));
			TwitchUser existing = chat.getUsers().get(winner.getUsername());
			int winnings = (int) (winner.getPoints() * 1.5);
			if (existing != null) {
				existing.addPoints(winnings);
			}
			writeToChat(winner.getUsername() + " has won the battle arena! The winner gets 150% of their bet. " + winnings + " " + chat.getSettings().getPointName() + ".");
		}
		waitTimer = 0;
		active = false;
		users.clear();
		lastBattleTime = System.currentTimeMillis();
	}

	public void writeToChat(String message) {
		try {
			chat.sendMessage(chat.getChannels()[0], "[Battle Arena] " + message);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public long getLastBattleTime() {
		return lastBattleTime;
	}

}
