package com.arithium.twitchbot.frame;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.SystemColor;
import java.lang.reflect.Method;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import com.arithium.twitchbot.BotBootstrap;
import com.arithium.twitchbot.BotSettings;
import com.arithium.twitchbot.ChannelContainer;
import com.arithium.twitchbot.chat.TwitchChat;

/**
 * Contains the login frame drawing
 * 
 * @author Arithium
 *
 */
public class LoginFrame extends JFrame {

	/**
	 * A text field containing the username box
	 */
	public JTextField usernameTextBox;

	/**
	 * A password field containing the oauth password box
	 */
	public JPasswordField oAuthTextBox;

	/**
	 * A text field containing the client id text box
	 */
	public JTextField clientIdTextBox;

	/**
	 * A channel text box
	 */
	public JTextField channelTextBox;

	/**
	 * A radio button to determine if the bot settings should be saved or not
	 */
	private JRadioButton saveRadioButton = new JRadioButton("Save Connection Information");;

	/**
	 * Constructs a new {@link LoginFrame}
	 */
	public LoginFrame() {
		setResizable(false);
		setTitle("Arithium's Twitch Bot");
		Insets insets = getInsets();
		setSize(320 + insets.right + insets.left, 278 + insets.top + insets.bottom);
		getContentPane().setBackground(Color.WHITE);
		setBackground(Color.WHITE);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Panel panel = new Panel();
		panel.setFont(new Font("Dialog", Font.PLAIN, 8));
		panel.setBackground(SystemColor.control);
		panel.setBounds(0, 0, 320, 250);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel usernameLabel = new JLabel("Twitch Username:");
		usernameLabel.setBounds(10, 30, 100, 25);
		usernameLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		panel.add(usernameLabel);
		usernameLabel.setForeground(SystemColor.desktop);

		usernameTextBox = new JTextField();
		usernameTextBox.setBounds(110, 30, 180, 25);
		panel.add(usernameTextBox);
		usernameTextBox.setColumns(10);

		JLabel oAuthLabel = new JLabel("Twitch OAuth:");
		oAuthLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		oAuthLabel.setBounds(10, 65, 100, 25);
		panel.add(oAuthLabel);
		oAuthLabel.setForeground(SystemColor.desktop);

		oAuthTextBox = new JPasswordField();
		oAuthTextBox.setBounds(110, 65, 180, 25);
		panel.add(oAuthTextBox);
		oAuthTextBox.setColumns(10);

		JLabel clientIdLabel = new JLabel("Twitch Client Id:");
		clientIdLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		clientIdLabel.setBounds(10, 100, 100, 25);
		panel.add(clientIdLabel);
		clientIdLabel.setForeground(SystemColor.desktop);

		clientIdTextBox = new JTextField();
		clientIdTextBox.setBounds(110, 100, 180, 25);
		panel.add(clientIdTextBox);
		clientIdTextBox.setColumns(10);

		JLabel channelLabel = new JLabel("Twitch Channel:");
		channelLabel.setBackground(SystemColor.desktop);
		channelLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		channelLabel.setBounds(10, 135, 100, 25);
		panel.add(channelLabel);
		channelLabel.setForeground(SystemColor.desktop);

		channelTextBox = new JTextField();
		channelTextBox.setBounds(110, 135, 180, 25);
		panel.add(channelTextBox);
		channelTextBox.setColumns(10);

		JLabel EnterDetailsLabel = new JLabel("Please enter your twitch bot login information.");
		EnterDetailsLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		EnterDetailsLabel.setBounds(20, -1, 290, 20);
		panel.add(EnterDetailsLabel);
		EnterDetailsLabel.setForeground(SystemColor.desktop);

		Panel buttonPanel = new Panel();
		buttonPanel.setBounds(10, 195, 290, 33);
		panel.add(buttonPanel);

		JButton btnGetClientId = new JButton("Get Client Id");
		btnGetClientId.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnGetClientId.setBackground(SystemColor.menu);
		btnGetClientId.setToolTipText("Link to get your twitch account client Id.");
		btnGetClientId.addActionListener(ae -> {
			openURL("https://www.twitch.tv/settings/connections");
		});
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnGenerateOauth = new JButton("Generate OAuth");
		btnGenerateOauth.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnGenerateOauth.setToolTipText("Link to generate an OAuth for your twitch password.");
		btnGenerateOauth.setForeground(Color.BLACK);
		btnGenerateOauth.setBackground(SystemColor.menu);
		buttonPanel.add(btnGenerateOauth);
		btnGenerateOauth.addActionListener(ae -> {
			openURL("https://twitchapps.com/tmi/");
		});

		JButton btnLogin = new JButton("Login");
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnLogin.setBackground(SystemColor.menu);
		btnLogin.setToolTipText("Login to the bot channel.");
		btnLogin.addActionListener(ae -> {
			handleLogin();
		});
		buttonPanel.add(btnLogin);
		buttonPanel.add(btnGetClientId);

		JLabel informationLabel = new JLabel("Please note that no information is collected.");
		informationLabel.setFont(new Font("Tahoma", Font.ITALIC, 11));
		informationLabel.setBounds(49, 235, 241, 14);
		panel.add(informationLabel);
		informationLabel.setForeground(SystemColor.desktop);

		saveRadioButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
		saveRadioButton.setBounds(10, 175, 240, 23);
		panel.add(saveRadioButton);
		saveRadioButton.setForeground(SystemColor.desktop);
		saveRadioButton.setBackground(SystemColor.control);

		BotSettings settings = BotBootstrap.getBoostrap().getSettings();
		if (settings != null) {
			usernameTextBox.setText(settings.getUsername());
			oAuthTextBox.setText(settings.getOAuth());
			clientIdTextBox.setText(settings.getClientId());
			saveRadioButton.setSelected(BotBootstrap.getBoostrap().isRememberSettings());
		}

		setVisible(true);

	}

	/**
	 * Handles the login for the bot
	 */
	private void handleLogin() {
		BotLoginFrame.authenticated = true;
		if (usernameTextBox.getText().isEmpty() || oAuthTextBox.getText().isEmpty() || channelTextBox.getText().isEmpty() || clientIdTextBox.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Please enter valid login information.", "Enter Valid Information", JOptionPane.WARNING_MESSAGE);
			return;
		}

		TwitchChat chat = BotBootstrap.getBoostrap().start(usernameTextBox.getText(), oAuthTextBox.getText(), channelTextBox.getText());

		if (chat == null) {
			JOptionPane.showMessageDialog(null, "Failed to authenticate your account, check your login info.", "Failed Authentication", JOptionPane.WARNING_MESSAGE);
			return;
		}
		BotBootstrap.getBoostrap().setBotFrame(this, new BotClientFrame());
		ChannelContainer.load();
		BotBootstrap.getBoostrap().setRememberSettings(saveRadioButton.isSelected());
	}

	/**
	 * Opens a URL to a website with the default browser
	 * 
	 * @param url
	 *            The url to open up in a browser
	 */
	private void openURL(String url) {
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Mac OS")) {
				Class<?> fileMgr = Class.forName("com.apple.eio.FileManager");
				Method openURL = fileMgr.getDeclaredMethod("openURL", new Class[] { String.class });
				openURL.invoke(null, new Object[] { url });
			} else if (osName.startsWith("Windows"))
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
			else { // assume Unix or Linux
				String[] browsers = { "firefox", "opera", "konqueror", "epiphany", "mozilla", "netscape", "safari" };
				String browser = null;
				for (int count = 0; count < browsers.length && browser == null; count++)
					if (Runtime.getRuntime().exec(new String[] { "which", browsers[count] }).waitFor() == 0)
						browser = browsers[count];
				if (browser == null) {
					throw new Exception("Could not find web browser");
				} else
					Runtime.getRuntime().exec(new String[] { browser, url });
			}
		} catch (Exception e) {
		}
	}
}
