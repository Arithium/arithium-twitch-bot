package com.arithium.twitchbot.frame;

import java.lang.reflect.Method;

import javax.swing.JFrame;

/**
 * The login frame for the bot
 * 
 * @author Arithium
 *
 */
public class BotLoginFrame extends BotFrame {

	/**
	 * The bot was authenticated when attempting to login
	 */
	public static boolean authenticated = true;

	/**
	 * The jframe for the bot frame
	 */
	private static JFrame frame;

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public void build() {
		frame = new LoginFrame();
	}

	@Override
	public JFrame getFrame() {
		return frame;
	}

}
