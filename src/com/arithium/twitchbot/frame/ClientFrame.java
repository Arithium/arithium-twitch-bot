package com.arithium.twitchbot.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Panel;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneLayout;
import javax.swing.border.BevelBorder;

import com.arithium.twitchbot.BotBootstrap;
import com.arithium.twitchbot.ChannelContainer;
import com.arithium.twitchbot.ChatEngine;
import com.arithium.twitchbot.chat.TwitchChat;

/**
 * Our JFrame for the client frame that displays our list of connected channels
 * 
 * @author Arithium
 *
 */
public class ClientFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * A JList to store the list of channels
	 */
	public static JList<String> list;

	/**
	 * Our default list model to store the elements in the list
	 */
	private static DefaultListModel<String> listModel;

	/**
	 * A text field which you can insert channels in
	 */
	private JTextField textField;

	public ClientFrame() {
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		setSize(300, 320);
		setTitle("Arithium's Twitch Bot");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setSize(274, 198);
		scrollPane.setLocation(10, 23);
		scrollPane.setLayout(new ScrollPaneLayout());
		listModel = new DefaultListModel<String>();
		listModel.addElement(BotBootstrap.getBoostrap().getSettings().getUsername());

		list = new JList<String>(listModel);
		list.setFont(new Font("Tahoma", Font.PLAIN, 14));
		list.setBackground(SystemColor.controlHighlight);
		list.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(list);
		getContentPane().add(scrollPane);

		JLabel lblListOfConnected = new JLabel("List of connected channels");
		lblListOfConnected.setBounds(25, 3, 262, 15);
		lblListOfConnected.setFont(new Font("Trajan Pro", Font.PLAIN, 14));
		getContentPane().add(lblListOfConnected);

		Panel panel = new Panel();
		panel.setLocation(0, 231);
		panel.setSize(this.getWidth(), 60);
		panel.setBackground(SystemColor.controlHighlight);
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("Add Channel");
		btnNewButton.setBackground(new Color(240, 240, 240));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String channel = textField.getText();

				if (channel != null && !channel.isEmpty()) {
					
					if (channel.equalsIgnoreCase("!shutdown")) {
						ChatEngine.shutdown = true;
						return;
					}

					TwitchChat chat = BotBootstrap.getBoostrap().start(BotBootstrap.getBoostrap().getSettings().getUsername(), BotBootstrap.getBoostrap().getSettings().getOAuth(), channel);
					if (chat != null) {
						addToList(channel);
						ChannelContainer.addChannel(channel, chat);
						textField.setText("");
						ChannelContainer.save();
					} else {
						JOptionPane.showMessageDialog(null, "Failed to connect to the twitch channel.", "Failure.", JOptionPane.WARNING_MESSAGE);
					}
				}
			}
		});
		panel.add(btnNewButton);

		JButton btnRemoveSelectedChannel = new JButton("Remove Selected Channel");
		btnRemoveSelectedChannel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRemoveSelectedChannel.setBackground(new Color(240, 240, 240));
		btnRemoveSelectedChannel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (list.getSelectedValue() == null) {
					return;
				}

				String channel = String.valueOf(list.getSelectedValue());
				removeFromList(channel);
				TwitchChat chat = ChannelContainer.getChannels().get(channel);

				if (chat != null) {
					chat.disconnect();
				}
				ChannelContainer.removeChannel(channel);
				ChannelContainer.save();
			}
		});
		panel.add(btnRemoveSelectedChannel);
	}

	/**
	 * Adds a new element to the list of connected channels
	 * 
	 * @param channel
	 *            The name of the channel to add to the list
	 */
	public static void addToList(String channel) {
		listModel.addElement(channel);
	}

	/**
	 * Removes an element from the list of connected channels
	 * 
	 * @param channel
	 *            The name of the channel to remove from the list
	 */
	public static void removeFromList(String channel) {

		if (channel.equalsIgnoreCase(BotBootstrap.getBoostrap().getSettings().getUsername())) {
			return;
		}
		listModel.removeElement(channel);
	}

}
