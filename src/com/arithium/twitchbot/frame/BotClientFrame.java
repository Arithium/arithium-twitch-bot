package com.arithium.twitchbot.frame;

import javax.swing.JFrame;

import com.arithium.twitchbot.chat.TwitchChat;

/**
 * The actual of the bot client
 * 
 * @author Arithium
 *
 */
public class BotClientFrame extends BotFrame {

	/**
	 * Chat stuff
	 */

	private static TwitchChat twitchChat;

	/**
	 * The client frame of the bot
	 */
	private ClientFrame frame;

	/**
	 * Default serialized version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Builds a new JFrame for this {@link BotFrame}
	 */
	@Override
	public void build() {
		frame = new ClientFrame();
	}

	@Override
	public JFrame getFrame() {
		return frame;
	}

}
