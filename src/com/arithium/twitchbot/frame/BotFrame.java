package com.arithium.twitchbot.frame;

import javax.swing.JFrame;

/**
 * Represents a single bot frame instance
 * 
 * @author Arithium
 *
 */
public abstract class BotFrame {

	/**
	 * Builds the frame
	 */
	public abstract void build();

	/**
	 * Constructs a new botframe instance
	 */
	public BotFrame() {
	}

	/**
	 * Gets the {@link JFrame} for this frame instance
	 * 
	 * @return The frame for this bot frame instance
	 */
	public abstract JFrame getFrame();

}
