package com.arithium.twitchbot;

import java.io.IOException;

import javax.swing.JFrame;

import org.jibble.pircbot.IrcException;

import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.frame.BotClientFrame;
import com.arithium.twitchbot.frame.BotFrame;
import com.arithium.twitchbot.frame.BotLoginFrame;
import com.arithium.twitchbot.point.TwitchUserContainer;

/**
 * The boostrap for the bot client which will hold the chat instance
 * 
 * @author Arithium
 *
 */
public class BotBootstrap {

	/**
	 * An instance for referencing the {@link BotBoostrap}
	 */
	private static BotBootstrap boostrap = new BotBootstrap();

	/**
	 * The current frame of the client
	 */
	public BotFrame frame;

	/**
	 * Our current chat instance for the bot itself
	 */
	private TwitchChat chat;

	/**
	 * The settings for the bot client
	 */
	private BotSettings settings = new BotSettings("", "", "", "points");

	/**
	 * Should the bot remember settings
	 */
	private boolean rememberSettings;

	/**
	 * Logs into the chat client
	 * 
	 * @param login
	 *            The username to log in with
	 * @param password
	 *            The password to login with
	 * @param channel
	 *            The Channel to connect too
	 * @return
	 */
	public TwitchChat start(String login, String password, String channel) {
		
		System.out.println("Attempting to join channel: #" + channel);
		if (ChannelContainer.getChannels().containsKey(channel)) {
			return null;
		}
		
		chat = new TwitchChat(login);
		chat.setVerbose(false);

		try {
			chat.connect("irc.chat.twitch.tv", 6667, password);
		} catch (IOException | IrcException e1) {
			e1.printStackTrace();
			return null;
		}
		
		if (!BotLoginFrame.authenticated) {
			return null;
		}

		chat.joinChannel("#" + channel);
		chat.getUsers().load(channel);
		ChannelContainer.addChannel(channel, chat);
		chat.loadChatSettings(channel);
		System.out.println("Successfully joined the channel #" + channel + ".");
		return chat;
	}

	/**
	 * Builds the bot client
	 */
	public void build() {
		System.out.println("Starting up the bot client.");
		try {
			BotFunctions.readBotSettings();
		} catch (Exception e) {
			e.printStackTrace();
		}
		setBotFrame(null, new BotLoginFrame());
		new ChatEngine();
	}

	/**
	 * Sets the bot frame and builds it
	 * 
	 * @param frame
	 */
	public void setBotFrame(JFrame currentFrame, BotFrame newFrame) {
		if (currentFrame != null) {
			currentFrame.setVisible(false);
			currentFrame.dispose();
		}
		
		this.frame = newFrame;
		frame.build();
	}

	/**
	 * Gets the boostrap instance of this bot
	 * 
	 * @return The {@link BotBoostrap} instance
	 */
	public static BotBootstrap getBoostrap() {
		return boostrap;
	}

	/**
	 * Gets the current {@link TwitchChat} instance
	 * 
	 * @return The current chat instance
	 */
	public TwitchChat getChat() {
		return chat;
	}

	/**
	 * Gets the frame of the bot client
	 * 
	 * @return The {@link BotFrame} of the client
	 */
	public BotFrame getFrame() {
		return frame;
	}

	/**
	 * Gets the bot settings
	 * 
	 * @return
	 */
	public BotSettings getSettings() {
		return settings;
	}

	/**
	 * Sets the bot settings
	 * 
	 * @param settings
	 *            The {@link BotSettings} to save
	 */
	public void setBotSettings(BotSettings settings) {
		this.settings = settings;
	}

	/**
	 * Should the bot remember settings
	 * 
	 * @return
	 */
	public boolean isRememberSettings() {
		return rememberSettings;
	}

	/**
	 * Set the bot to remember settings
	 * 
	 * @param remember
	 */
	public void setRememberSettings(boolean remember) {
		this.rememberSettings = remember;
	}

}
