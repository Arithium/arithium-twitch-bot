package com.arithium.twitchbot;

/**
 * Represents the settings for the bot client
 * 
 * @author Arithium
 *
 */
public class BotSettings {

	/**
	 * The username of the bot
	 */
	private String username;

	/**
	 * The oAuth of the login
	 */
	private String oAuth;

	/**
	 * The client id of the both
	 */
	private String clientId;

	/**
	 * The name of the points
	 */
	private String pointName;

	/**
	 * Consturcts a new {@link BotSettings} system
	 * 
	 * @param username
	 *            The username to login with
	 * @param oAuth
	 *            The oAuth password to login with
	 * @param clientId
	 *            The client id of the twitch user
	 * @param pointName
	 *            The name of the points
	 */
	public BotSettings(String username, String oAuth, String clientId, String pointName) {
		this.username = username;
		this.oAuth = oAuth;
		this.clientId = clientId;
		this.pointName = pointName;
	}

	/**
	 * Gets the username of the login
	 * 
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Gets the generated oAuth to login
	 * 
	 * @return
	 */
	public String getOAuth() {
		return oAuth;
	}

	/**
	 * Gets the client id of the user
	 * 
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * Gets the name of the point system
	 * 
	 * @return The name of the point system
	 */
	public String getPointName() {
		return pointName;
	}

	@Override
	public String toString() {
		return "Username = " + username + ", OAuth = " + oAuth + ", ClientId = " + clientId;
	}

}
