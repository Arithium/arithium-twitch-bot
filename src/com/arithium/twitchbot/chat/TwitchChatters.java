package com.arithium.twitchbot.chat;

/**
 * An object used to fetch the chatters in a specific {@link TwitchChat}
 * 
 * @author Arithium
 *
 */
public class TwitchChatters {

	/**
	 * An array containing the moderators of the channel
	 */
	private String[] moderators;

	/**
	 * An array containing the staff members in the channel
	 */
	private String[] staff;

	/**
	 * An array containing administrators of the channel
	 */
	private String[] admins;

	/**
	 * An array containing global moderators in the channel
	 */
	private String[] global_mods;

	/**
	 * An array containing viewers in the channel
	 */
	private String[] viewers;

	/**
	 * Constructs a new {@link TwitchChatters} instance
	 * 
	 * @param moderators
	 *            The moderators in the channel
	 * @param staff
	 *            The staff members in the channel
	 * @param admins
	 *            The admins in the channel
	 * @param global_mods
	 *            The global mods in the channel
	 * @param viewers
	 *            The viewers in the channel
	 */
	public TwitchChatters(String[] moderators, String[] staff, String[] admins, String[] global_mods, String[] viewers) {
		this.moderators = moderators;
		this.staff = staff;
		this.admins = admins;
		this.global_mods = global_mods;
		this.viewers = viewers;
	}

	/**
	 * Gets the moderators in the channel
	 * 
	 * @return The moderators in the channel
	 */
	public String[] getModerators() {
		return moderators;
	}

	/**
	 * Gets the staff members in the channel
	 * 
	 * @return The staff members in the channel
	 */
	public String[] getStaff() {
		return staff;
	}

	/**
	 * Gets the admins in the channel
	 * 
	 * @return The admins in the channel
	 */
	public String[] getAdmins() {
		return admins;
	}

	/**
	 * Gets the global mods in the channel
	 * 
	 * @return The global mods in the channel
	 */
	public String[] getGlobalMods() {
		return global_mods;
	}

	/**
	 * Gets the viewers in the channel
	 * 
	 * @return The viewers in the channel
	 */
	public String[] getViewers() {
		return viewers;
	}

}
