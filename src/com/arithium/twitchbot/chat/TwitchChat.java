package com.arithium.twitchbot.chat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jibble.pircbot.PircBot;

import com.arithium.twitchbot.battle.BattleArena;
import com.arithium.twitchbot.bets.BetManager;
import com.arithium.twitchbot.chat.commands.CommandExecutors;
import com.arithium.twitchbot.frame.BotLoginFrame;
import com.arithium.twitchbot.point.TwitchUserContainer;
import com.arithium.twitchbot.quest.QuestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Represents a single Chat instance between the bot and a channel
 * 
 * @author Arithium
 *
 */
public class TwitchChat extends PircBot {

	/**
	 * The delay in which to refresh the chat
	 */
	private int refreshDelay = -1;

	/**
	 * A quest handler to handle quests specific to this chat
	 */
	private QuestHandler questHandler;

	private BattleArena battleArena = new BattleArena(this);
	
	private BetManager betManager = new BetManager(this);

	private ChatSettings settings;

	/**
	 * A container of users who have been in this chat, stores information such
	 * as points and time in chat
	 */
	private TwitchUserContainer users = new TwitchUserContainer();

	/**
	 * Constructs a new {@link TwitchChat} instance between the bot and a
	 * channel
	 * 
	 * @param username
	 *            The bot nickname to connect to the channel with
	 */
	public TwitchChat(String username) {
		this.setName(username);
		questHandler = new QuestHandler(this);
		defaultSettings();
	}

	@Override
	public void onChannelInfo(String channel, int userCount, String topic) {
		System.out.println("Info: " + channel + " : " + userCount + " : " + topic);
	}

	@Override
	public void onNotice(String sourceNick, String sourceLogin, String sourceHostname, String target, String notice) {
		System.out.println("Notice: " + sourceNick + " : " + sourceLogin + " : " + sourceHostname + " : " + target
				+ " : " + notice);
		if (notice.contains("failed") || notice.contains("Improperly")) {
			BotLoginFrame.authenticated = false;
		}
	}

	@Override
	public void onJoin(String channel, String sender, String login, String hostname) {
	}

	@Override
	public void onPart(String channel, String sender, String login, String hostname) {
	}

	@Override
	public void onAction(String sender, String login, String hostname, String target, String action) {
	}

	@Override
	public void onMessage(String channel, String sender, String login, String hostname, String message) {
//		System.out.println("Message: " + channel + " : " + login + " : " + hostname + " : " + message);

		if (!sender.equalsIgnoreCase("arithium1")) {
			// return;
		}
		if (message.startsWith("!")) {
			CommandExecutors.submit(sender, message, this, channel);
		}
	}

	/**
	 * Saves the user data for the channel
	 * 
	 * @param channel
	 *            The name of the channel
	 */
	public void saveChatSettings(String channel) {
		Path path = Paths.get("./data" + "/channels/" + channel.replaceAll("#", ""));

		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		
		Path file = Paths.get(path.toFile().getPath() + "/chat_settings.json");
		
		if (!Files.exists(file)) {
			try {
				Files.createFile(path);
			} catch (IOException e) {
//				e.printStackTrace();
			}
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		try {
			BufferedWriter writer = Files.newBufferedWriter(file);
			writer.write(gson.toJson(settings, ChatSettings.class));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void defaultSettings() {
		settings = new ChatSettings();
		settings.setPointCommandName("points");
		settings.setPointName("points");
	}

	/**
	 * Loads the user data for the channel
	 * 
	 * @param channel
	 *            The name of the channel
	 */
	public void loadChatSettings(String channel) {
		Path path = Paths.get("./data" + "/channels/" + channel.replaceAll("#", ""));

		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		
		Path file = Paths.get(path.toFile().getPath() + "/chat_settings.json");
		
		if (!Files.exists(file)) {
			saveChatSettings(channel);
			return;
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try {
			BufferedReader reader = Files.newBufferedReader(file);
			settings = (gson.fromJson(reader, ChatSettings.class));
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Resets the refresh delay back to 0
	 */
	public void resetRefreshDelay() {
		refreshDelay = 0;
	}

	/**
	 * Gets the refresh delay of the chat
	 * 
	 * @return The refresh delay for the chat
	 */
	public int getRefreshDelay() {
		return refreshDelay;
	}

	/**
	 * Ticks up the refresh delay
	 */
	public void tickRefreshDelay() {
		refreshDelay++;
	}

	/**
	 * Gets the {@link QuestHandler} instance to handle quests for this chat
	 * 
	 * @return The quest handler instance for this chat
	 */
	public QuestHandler getQuest() {
		return questHandler;
	}

	/**
	 * Sets the user container for this chat
	 * 
	 * @param container
	 *            The {@link TwitchUserContainer} related to this chat
	 */
	public void setUsers(TwitchUserContainer container) {
		this.users = container;
	}

	/**
	 * Gets the {@link TwitchUserContainer} related to this chat
	 * 
	 * @return The user container related to this chat
	 */
	public TwitchUserContainer getUsers() {
		return users;
	}

	public BattleArena getBattleArena() {
		return battleArena;
	}
	
	public ChatSettings getSettings() {
		return settings;
	}
	
	public BetManager getBetManager() {
		return betManager;
	}

}
