package com.arithium.twitchbot.chat;

public class ChatSettings {

	/**
	 * The name of the points
	 */
	private String pointName = "points";

	private String pointCommandName = "points";

	/**
	 * Sets the name of the points
	 * 
	 * @param name
	 */
	public void setPointName(String name) {
		this.pointName = name;
	}

	/**
	 * The name of the points
	 * 
	 * @return
	 */
	public String getPointName() {
		return pointName;
	}

	/**
	 * Sets the point command name
	 * 
	 * @param name
	 */
	public void setPointCommandName(String name) {
		this.pointCommandName = name;
	}

	/**
	 * Gets the point command name
	 * 
	 * @return
	 */
	public String getPointCommand() {
		return pointCommandName;
	}

}
