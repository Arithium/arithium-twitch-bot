package com.arithium.twitchbot.chat;

import java.util.Date;

/**
 * Represents a twitch follow object
 * 
 * @author Arithium
 *
 */
public class TwitchFollowObject {

	/**
	 * The {@link Date} which the person followed the channel
	 */
	private Date created_at;

	/**
	 * Constructs a new {@link TwitchFollowObject}
	 * 
	 * @param created_at
	 *            The {@link Date} which the person followed the channel
	 */
	public TwitchFollowObject(Date created_at) {
		this.created_at = created_at;
	}

	/**
	 * Gets the date the person followed the channel
	 * 
	 * @return The {@link Date} the person followed the channel
	 */
	public Date getDate() {
		return created_at;
	}

}
