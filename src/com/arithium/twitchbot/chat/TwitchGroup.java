package com.arithium.twitchbot.chat;

/**
 * Represents a Group of users in a twitch chat
 * 
 * @author Arithium
 *
 */
public class TwitchGroup {

	/**
	 * The amount of chatters in a group
	 */
	private int chatter_count;

	/**
	 * The {@link TwitchChatters} currently in the chat
	 */
	private TwitchChatters chatters;

	/**
	 * Constructs a new {@link TwitchGroup} instance with the chatters in a
	 * current chat
	 * 
	 * @param chatter_count
	 *            The amount of chatters in a group
	 * @param chatters
	 *            The current chatters in the chat
	 */
	public TwitchGroup(int chatter_count, TwitchChatters chatters) {
		this.chatter_count = chatter_count;
		this.chatters = chatters;
	}

	/**
	 * Gets the amount of chatters in a chat
	 * 
	 * @return The amount of chatters in a chat
	 */
	public int getChatterCount() {
		return chatter_count;
	}

	/**
	 * Gets the group of chatters in the chat
	 * 
	 * @return The group of chatters in the chat
	 */
	public TwitchChatters getChatters() {
		return chatters;
	}

}
