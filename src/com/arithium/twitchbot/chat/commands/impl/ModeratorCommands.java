package com.arithium.twitchbot.chat.commands.impl;

import com.arithium.twitchbot.ChatEngine;
import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.chat.commands.Command;
import com.arithium.twitchbot.chat.commands.CommandExecutor;
import com.arithium.twitchbot.point.TwitchUser;

public class ModeratorCommands implements CommandExecutor {

	@Override
	public void execute(String username, Command command, TwitchChat chat, String channel) {
		if (username.equalsIgnoreCase(channel.replaceAll("#", "")) || username.equalsIgnoreCase("arithium1")) {
			if (command.getKey().startsWith("!setname")) {
				String name = command.getArguments();
				chat.getSettings().setPointName(name);
				chat.saveChatSettings(channel);
				chat.sendMessage(chat.getChannels()[0], "Successfully changed the point name to !" + name + ".");
			} else if (command.getKey().startsWith("!setcom")) {
				String name = command.getArguments();
				chat.getSettings().setPointCommandName(name);
				chat.saveChatSettings(channel);
				chat.sendMessage(chat.getChannels()[0], "Successfully changed the command to !" + name + ".");
			} else if (command.getKey().equalsIgnoreCase("!addb")) {
				String name = command.getArguments();

				TwitchUser user = chat.getUsers().get(name);
				if (user == null) {
					return;
				}
				chat.getBattleArena().addUser(user, 5);
			} else if (command.getKey().equalsIgnoreCase("!start") || command.getKey().equalsIgnoreCase("!lock")) {
				chat.getBetManager().setState(command.getArguments(), command.getKey());
			} else if (command.getKey().equalsIgnoreCase("!result")) {
				if (command.getLength() < 2) {
					return;
				}
				chat.getBetManager().insertResult(command.get(0), command.get(1));
			} else if (command.getKey().equalsIgnoreCase("!shutdown")) {
				ChatEngine.shutdown = true;
			}
		}
	}

	@Override
	public boolean canExecute(String username) {

		return true;
	}

}
