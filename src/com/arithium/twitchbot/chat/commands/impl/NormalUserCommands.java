package com.arithium.twitchbot.chat.commands.impl;

import java.io.IOException;

import org.json.JSONException;

import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.bets.Bet;
import com.arithium.twitchbot.bets.BetManager;
import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.chat.TwitchFollowObject;
import com.arithium.twitchbot.chat.commands.Command;
import com.arithium.twitchbot.chat.commands.CommandExecutor;
import com.arithium.twitchbot.point.TwitchUser;

public class NormalUserCommands implements CommandExecutor {

	@Override
	public void execute(String username, Command command, TwitchChat chat, String channel) {
		if (command.getKey().equalsIgnoreCase("!" + chat.getSettings().getPointCommand() + "")) {
			TwitchUser point = chat.getUsers().get(username);
			if (point != null) {
				int total = point.getMinutes();
				int days = total / (24 * 60);
				int hours = (total % (24 * 60)) / 60;
				int minutes = (total % (24 * 60)) % 60;
				String dayTime = ((days > 0) ? (days + " days ") : "");
				String hourTime = ((hours > 0) ? (hours + " hours ") : "");
				String minuteTime = ((minutes != 1 ? (minutes + " minutes") : (minutes + " minute")));
				String messageToSend = username + " has " + point.getPoints() + " "
						+ chat.getSettings().getPointName() + ". " + dayTime + " " + hourTime + ""
						+ minuteTime + ".";
				chat.sendMessage(channel, messageToSend);
			} else {
				String messageToSend = username + " has 0 " + chat.getSettings().getPointName()
						+ ".";
				chat.sendMessage(channel, messageToSend);
			}

		} else if (command.getKey().equalsIgnoreCase("!arfollow")) {

			TwitchFollowObject object = null;
			try {
				object = BotFunctions.readFollow(chat, username);
			} catch (IOException | JSONException e) {
				object = null;
			}

			String messageToSend = (object == null ? (username + " is not following.")
					: (username + " followed on " + object.getDate()) + ".");
			chat.sendMessage(channel, messageToSend);
		} else if (command.getKey().equalsIgnoreCase("!amicool")) {
			String messageToSend = "Yes you are " + username + ".";
			chat.sendMessage(channel, messageToSend);

		} else if (command.getKey().equalsIgnoreCase("!ff7fact")) {
			String[] facts = new String[] { "Final Fantasy 7 was released on January 31, 1997.", "Cloud Sucks..",
					"Sephiroth is impossible to kill in battle.", "Cloud and Zack were experimented on by Hojo." };
			int random = BotFunctions.random(facts.length - 1);
			String fact = facts[random];
			chat.sendMessage(channel, fact);
		} else if (command.getKey().equalsIgnoreCase("!quest")) {
			if (System.currentTimeMillis() - chat.getQuest().cachedTime() < 200_000) {
				String m = "The next quest has not yet started.";
				chat.sendMessage(channel, m);
				return;
			} else if (chat.getQuest().getActiveQuest()) {
				String m = "The quest has already started.";
				chat.sendMessage(channel, m);
				return;
			}

			if (command.getLength() < 1) {
				return;
			}

			int points = 0;
			double percentage = 0;

			try {

				if (command.get(0).contains("%")) {
					percentage = (Double.parseDouble(command.get(0).replace("%", "")) / 100);
				} else {
					points = command.getInt(0);
				}

				int existing = 0;

				TwitchUser person = chat.getUsers().get(username);

				if (person != null) {
					existing = person.getPoints();
				}

				if (percentage > 0) {
					points = (int) (existing * percentage);
				}

				if (points > existing) {
					points = existing;
				}
			} catch (NumberFormatException e) {
				points = 0;
			}

			if (points < 0) {
				return;
			}
			chat.getQuest().queueUser(new TwitchUser(username, points));
		} else if (command.getKey().toLowerCase().startsWith("!battle")) {
			
			if (command.getLength() < 1) {
				return;
			}
			
			TwitchUser person = chat.getUsers().get(username);
			
			if (person == null)
				return;

			int points = 0;
			double percentage = 0;

			try {

				if (command.get(0).contains("%")) {
					percentage = (Double.parseDouble(command.get(0).replace("%", "")) / 100);
				} else {
					points = command.getInt(0);
				}

				int existing = 0;

				if (person != null) {
					existing = person.getPoints();
				}

				if (percentage > 0) {
					points = (int) (existing * percentage);
				}

				if (points > existing) {
					points = existing;
				}
			} catch (NumberFormatException e) {
				points = 0;
			}

			if (points < 0) {
				return;
			}
			
			long delay = System.currentTimeMillis() - chat.getBattleArena().getLastBattleTime(); 
			if (delay < 300_000) {
				chat.sendMessage(chat.getChannels()[0], "They're still cleaning up the Battle Arena from the last match. Try again in another " + ((300_000 - delay) / 1000) + " seconds.");
				return;
			}
			
			
			chat.getBattleArena().addUser(person, points);
		} else if (command.getKey().equalsIgnoreCase("!jrm")) {
			String message = "Now this is a story all about how Jrm couldn't figure out the puzzles. And Id like to take a minute Just sit right there Ill tell you how he became a halfway decent zelda player.";
			chat.sendMessage(chat.getChannels()[0], message);
		} else if (command.getKey().equalsIgnoreCase("!info") || command.getKey().equalsIgnoreCase("!help")) {
			chat.sendMessage(channel, "/w " + username + " The available commands are. !" + chat.getSettings().getPointCommand() + " to get your current points. !battle <bet> to join in a battle. !quest <bet> to join in a quest. !amicool - to print a fun message.");
		} 
		
		Bet bet = BetManager.getBet(command.getKey().replaceAll("!", ""));
		
		if (bet != null) {
			if (command.getLength() < 2) {
				return;
			}
			
			TwitchUser person = chat.getUsers().get(username);
			
			if (person == null)
				return;
			
			String value = command.get(0);

			int points = 0;
			double percentage = 0;

			try {

				if (command.get(1).contains("%")) {
					percentage = (Double.parseDouble(command.get(1).replace("%", "")) / 100);
				} else {
					points = command.getInt(1);
				}

				int existing = 0;

				if (person != null) {
					existing = person.getPoints();
				}

				if (percentage > 0) {
					points = (int) (existing * percentage);
				}

				if (points > existing) {
					points = existing;
				}
			} catch (NumberFormatException e) {
				points = 0;
			}

			if (points < 0) {
				return;
			}
			
			chat.getBetManager().bet(person, bet, value, points);
		}
	}

}
