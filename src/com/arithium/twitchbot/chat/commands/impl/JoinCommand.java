package com.arithium.twitchbot.chat.commands.impl;

import java.util.Arrays;

import com.arithium.twitchbot.BotBootstrap;
import com.arithium.twitchbot.ChannelContainer;
import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.chat.commands.Command;
import com.arithium.twitchbot.chat.commands.CommandExecutor;
import com.arithium.twitchbot.frame.ClientFrame;

public class JoinCommand implements CommandExecutor {

	@Override
	public void execute(String username, Command command, TwitchChat chat, String channel) {
		if (command.getKey().equalsIgnoreCase("!join")) {
			if (BotBootstrap.getBoostrap().start(BotBootstrap.getBoostrap().getSettings().getUsername(), BotBootstrap.getBoostrap().getSettings().getOAuth(), username) != null) {
				chat.sendMessage(channel, "Successfully joined channel " + username + ".");
				ChannelContainer.save();
				ClientFrame.addToList(username);
			}
		} else if (command.getKey().equalsIgnoreCase("!leave")) {
			
			TwitchChat c = ChannelContainer.getChannels().get(username);
			if (c != null) {
				ChannelContainer.removeChannel(username);
				c.disconnect();
				ChannelContainer.save();
				ClientFrame.removeFromList(username);
			}
		}
	}

}
