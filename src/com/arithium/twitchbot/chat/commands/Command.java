package com.arithium.twitchbot.chat.commands;

/**
 * Represents a single command send by a user
 *
 * @Author Arithium
 */
public class Command {

	/**
	 * The key to the command, the arguements of the command
	 */
	private String key, arguments;

	/**
	 * An array containing the arguements of the command
	 */
	private String[] argumentArray;

	/**
	 * Constructs a new {@link Command} that can be executed
	 * 
	 * @param command
	 *            The name of the command
	 */
	public Command(String command) {
		key = arguments = "";
		argumentArray = new String[0];

		String[] parts = command.split(" ", 2);
		if (parts.length > 0) {
			key = parts[0];
		}
		if (parts.length > 1) {
			arguments = parts[1];
			argumentArray = arguments.split(" ");
		}
	}

	/**
	 * Gets the key of the command
	 * 
	 * @return The key of the command
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Gets the arguments of the command
	 * 
	 * @return
	 */
	public String getArguments() {
		return arguments;
	}

	/**
	 * Gets an argument from the index
	 * 
	 * @param index
	 *            The index of the argument to get
	 * @return The argument for the specified index
	 */
	public String get(int index) {
		try {
			return argumentArray[index];
		} catch (ArrayIndexOutOfBoundsException excention) {
			return "";
		}
	}

	/**
	 * Gets an argument after a specific index
	 * 
	 * @param index
	 *            The index to get the argument after
	 * @return The argument after the index
	 */
	public String getAfter(int index) {
		return arguments.split(" ", index + 1)[index];
	}

	/**
	 * Gets an integer value from an arguement
	 * 
	 * @param index
	 *            The index to get the argument from
	 * @return An integer value from the arguement
	 */
	public int getInt(int index) {
		try {
			return Integer.parseInt(get(index));
		} catch (NumberFormatException exception) {
			return -1;
		}
	}

	/**
	 * Gets a boolean value from the arguments
	 * 
	 * @param index
	 *            The index value to get the boolean arguement
	 * @return A boolean value from the arguments
	 */
	public boolean getBoolean(int index) {
		try {
			return Boolean.parseBoolean(get(index));
		} catch (Throwable exception) {
			return false;
		}
	}

	/**
	 * Gets the amount of arguments in the command
	 * 
	 * @return The amount of arguments in the command
	 */
	public int getLength() {
		return argumentArray.length;
	}

}