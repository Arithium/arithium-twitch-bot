package com.arithium.twitchbot.chat.commands;

import com.arithium.twitchbot.chat.TwitchChat;

/**
 * Command executor implementation.
 *
 * @Author Arithium
 */
public interface CommandExecutor {

	/**
	 * Executes commands for a particular user
	 * 
	 * @param username
	 *            The username of the person submitting the command
	 * @param command
	 *            The command being submitted
	 */
	public abstract void execute(String username, Command command, TwitchChat chat, String channel);

	/**
	 * Checks if the person can execute the command
	 * 
	 * @param username
	 *            The username of the person submitting the command
	 * @return If the person can use the command
	 */
	default boolean canExecute(String username) {
		return true;
	}

}