package com.arithium.twitchbot.chat.commands;

import java.util.HashSet;
import java.util.Set;

import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.chat.commands.impl.JoinCommand;
import com.arithium.twitchbot.chat.commands.impl.ModeratorCommands;
import com.arithium.twitchbot.chat.commands.impl.NormalUserCommands;

/**
 * Command executors manager.
 *
 * @Author Arithium
 */
public class CommandExecutors {

	/**
	 * A set of command executors
	 */
	private static final Set<CommandExecutor> executors = new HashSet<>();

	/**
	 * If true, only debug commands will be loaded
	 */
	public static boolean DEBUG = true;

	public static void init() {
		executors.add(new NormalUserCommands());
		executors.add(new ModeratorCommands());
		executors.add(new JoinCommand());
	}
	
	static {
		reload();
	}

	/**
	 * Reloads the commands
	 */
	public static void reload() {
		executors.clear();
		init();
	}

	/**
	 * Submits a new command to be processed
	 * 
	 * @param username
	 *            The username of the person submitting the command
	 * @param commandString
	 *            The command line being sent
	 */
	public static void submit(String username, String commandString, TwitchChat chat, String channel) {
		Command command = new Command(commandString);
		for (CommandExecutor executor : executors) {
			if (executor.canExecute(username)) {
				executor.execute(username, command, chat, channel);
			}
		}
	}

}