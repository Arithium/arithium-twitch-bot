package com.arithium.twitchbot.bets;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.point.TwitchUser;

public class BetManager {

	private final TwitchChat chat;

	public BetManager(TwitchChat chat) {
		this.chat = chat;
	}

	private static Map<String, Bet> bets = new HashMap<>();
	
	static {
		bets.put("chocobo", new ChocoboBet());
	}

	public void setState(String key, String state) {
		boolean enabled = state.equalsIgnoreCase("!start");
		Bet bet = bets.get(key);

		if (bet != null) {
			bet.setStarted(enabled);
			chat.sendMessage(chat.getChannels()[0], "[Bet Manager] " + (enabled ? "Started" : "Locked") + " the " + key + " bets!");
		}
	}
	
	public void process() {
		
	}

	public void insertResult(String key, String result) {
		Bet bet = bets.get(key);
		if (bet != null) {
			if (bet.isStarted()) {
				return;
			}
			String winners = "The winners for the " + key + " bet are ";
			for (UserBet user : bet.getWinners(result)) {
				winners += user.getUser().getUsername() + ", ";
				user.getUser().addPoints((int) (user.getAmount() * 1.5));
			}
			winners += " congratulations.";
			chat.sendMessage(chat.getChannels()[0], "[Bet Manager] " + winners);
			bet.getBets().clear();
		}
	}

	public void bet(TwitchUser user, Bet bet, String answer, int amount) {
		if (!bet.isStarted()) {
			return;
		}
		user.removePoints(amount);
		bet.addUser(user, answer, amount);
		chat.sendMessage(chat.getChannels()[0], "/w " + user.getUsername() + " Added new bet for " + answer + " with a bet of " + amount + ".");
	}

	public static Bet getBet(String key) {
		return bets.get(key);
	}

}
