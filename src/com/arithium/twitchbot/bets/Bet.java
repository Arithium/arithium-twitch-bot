package com.arithium.twitchbot.bets;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.arithium.twitchbot.point.TwitchUser;

public class Bet {
	
	private boolean started;
	
	private List<UserBet> bets = new ArrayList<>();
	
	public List<UserBet> getWinners(String value) {
		return bets.stream().filter(i -> i.getKey().equalsIgnoreCase(value)).collect(Collectors.toList());
	}
	
	public void addUser(TwitchUser user, String value, int amount) {
		if (bets.stream().anyMatch(i -> i.getUser().getUsername().equalsIgnoreCase(user.getUsername()))) {
			return;
		}
		bets.add(new UserBet(user, value, amount));
	}
	
	public List<UserBet> getBets() {
		return bets;
	}
	
	public boolean isStarted() {
		return started;
	}
	
	public void setStarted(boolean started) {
		this.started = started;
	}

}
