package com.arithium.twitchbot.bets;

import com.arithium.twitchbot.point.TwitchUser;

public class UserBet {
	
	private TwitchUser user;
	
	private String value;
	
	private int amount;
	
	public UserBet(TwitchUser user, String value, int amount) {
		this.user = user;
		this.value = value;
		this.amount = amount;
	}
	
	public TwitchUser getUser() {
		return user;
	}
	
	public String getKey() {
		return value;
	}
	
	public int getAmount() {
		return amount;
	}

}
