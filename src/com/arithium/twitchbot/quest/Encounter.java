package com.arithium.twitchbot.quest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.point.TwitchUser;

/**
 * Represents a single encounter of an enemy
 * 
 * @author Arithium
 *
 */
public interface Encounter {

	/**
	 * The message sent when you encounter the enemy
	 * 
	 * @return The message sent when you encounter the enemy
	 */
	public String encounterMessage();

	/**
	 * Gets the message sent when the enemy attacks you
	 * 
	 * @return
	 */
	public String onAttackMessage();

	/**
	 * Gets the message sent when you fail to attack the enemy
	 * 
	 * @return
	 */
	public String getFailedAttackMessage();

	/**
	 * Handles the attack from the enemy for the provided quest
	 * 
	 * @param quest
	 *            The {@link Quest} to handle the attack for
	 */
	public default void handleAttack(Quest quest) {
		List<String> usernames = new ArrayList<>();

		TwitchUser user = null;
		for (Iterator<TwitchUser> it$ = quest.getAdventurers().iterator(); it$.hasNext();) {
			TwitchUser active = it$.next();
			String username = active.getUsername();

			int modifier = quest.getAdventurers().size() * 2;
			int random = BotFunctions.random(80);
			int chance = 70;
			
			if (user != null && BotFunctions.random(25) < 2) {
				quest.getQuestHandler().writeToChat(user.getUsername() + " sacrifices themself to save " + username + ".");
				quest.getAdventurers().remove(user);
				return;
			}
			
			System.out.println(random+":"+chance+":"+modifier);
			if (random > (chance + modifier)) {
				usernames.add(username);
				it$.remove();
			} else {
				user = active;
			}
		}
		
		if (usernames.size() > 0)
			quest.getQuestHandler().writeToChat(usernames + " forgets to dodge the attack.");
		else
			quest.getQuestHandler().writeToChat("The party successfully dodges the attack.");
	}

}
