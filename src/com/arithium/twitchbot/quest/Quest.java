package com.arithium.twitchbot.quest;

import java.util.ArrayList;
import java.util.List;

import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.point.TwitchUser;
import com.arithium.twitchbot.point.TwitchUserContainer;

/**
 * Represents an expandable quest class
 * 
 * @author Arithium
 *
 */
public abstract class Quest {

	/**
	 * The current stage of the quest encounter
	 */
	private EncounterStage stage;

	/**
	 * The timer to delay the actions
	 */
	private int timer;

	/**
	 * The initial message sent when the quest starts
	 */
	private String initialMessage;

	/**
	 * The name of the quest
	 */
	private String questName;

	/**
	 * A list of people currently in the quest party
	 */
	private List<TwitchUser> adventurers = new ArrayList<>();

	/**
	 * A list of people currently in the dead party
	 */
	private List<TwitchUser> deaths = new ArrayList<>();

	/**
	 * A list of encounters during the quest
	 */
	private List<Encounter> encounters = new ArrayList<>();

	/**
	 * Determines if the quest is running
	 */
	private boolean running = true;

	/**
	 * Gets the current enemy encounter
	 */
	private Encounter currentEncounter;

	/**
	 * Gets the current encounter stage of how many encounters you've completed
	 */
	private int encounterStage;

	/**
	 * The quest handler dealing with this quest
	 */
	private QuestHandler questHandler;

	/**
	 * Constructs a new {@link Quest} instance
	 * 
	 * @param questName
	 *            The name of the quest
	 * @param initialMessage
	 *            The message sent when the quest is started
	 */
	public Quest(String questName, String initialMessage) {
		this.questName = questName;
		this.initialMessage = initialMessage;
	}

	/**
	 * Resets the quest
	 */
	public void resetQuest() {
		deaths.clear();
		encounters.clear();
		adventurers.clear();
		populateEncounters();
		timer = encounterStage = 0;
		if (encounters != null)
			currentEncounter = encounters.get(0);
		stage = EncounterStage.ENCOUNTER;
	}

	/**
	 * Adds an encounter to the list of encounters for this quest
	 * 
	 * @param encounter
	 *            The {@link Encounter} to add to this quest
	 */
	public void addEncounter(Encounter... encounter) {
		for (Encounter e : encounter)
			encounters.add(e);
	}

	/**
	 * Handles the tick of the quest to process encounters at a fixed rate
	 * 
	 * @return If the quest is running
	 */
	public boolean tick() {

		try {
			timer++;
			// System.out.println("Quest Timer: " + timer + ", Adventurers: " +
			// adventurers);
			if (timer % 30 == 0) {
				if (adventurers.size() <= 0) {
					questHandler.writeToChat("All of the adventurers have died, better luck next time.");
					return false;
				}
				onNextCycle();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return running;
	}

	/**
	 * Stops the quest
	 */
	public void stop() {
		running = false;
	}

	/**
	 * Increments the encounter stage after completing the encounter
	 */
	public void incrimentEncounter() {

		/*
		 * Incriment the encounter state
		 */
		encounterStage++;

		/*
		 * The amount of encounters for this quest
		 */
		int size = encounters.size();

		/*
		 * The encounter state should send out the boss
		 */
		if (encounterStage == size) {
			questHandler.writeToChat("The party continues on the quest.");
			setCurrentEncounter(getBossEncounter());

			/*
			 * There are no more encounters, end the quest
			 */
		} else if (encounterStage > size) {
			setCurrentEncounter(null);

			/*
			 * Send out the next encounter
			 */
		} else {
			questHandler.writeToChat("The party continues on the quest.");
			setCurrentEncounter(encounters.get(encounterStage));
		}
	}

	/**
	 * Handles the next cycle of the quest
	 */
	public void onNextCycle() {
		Encounter current = getCurrentEncounter();

		if (current == null) {
			stop();
			return;
		}

		switch (getStage()) {

		case ENCOUNTER:
			questHandler.writeToChat(current.encounterMessage());

			if (BotFunctions.random(50) < 20) {
				setStage(EncounterStage.ATTACKING_MESSAGE);
			} else {
				setStage(EncounterStage.ATTACK_ENEMY);
			}
			break;

		case ATTACKING_MESSAGE:
			questHandler.writeToChat(current.onAttackMessage());
			setStage(EncounterStage.ATTACKED_MESSAGE);
			break;

		case ATTACK_ENEMY:
			if (BotFunctions.random(20) < 4) {
				extraAttackOptions();
			} else {
				questHandler.writeToChat("The party launches an attack on the enemy.");
				setStage(EncounterStage.HANDLE_ATTACK);
			}
			break;

		case ATTACKED_MESSAGE:
			current.handleAttack(this);
			if (BotFunctions.random(50) < 35) {
				setStage(EncounterStage.ATTACK_ENEMY);
			} else {
				setStage(EncounterStage.ATTACKING_MESSAGE);
			}
			break;

		case REGROUPING:
			if (BotFunctions.random(20) < 4) {
				extraAttackOptions();
			} else {
				questHandler.writeToChat("The party regroups and launches a counter attack.");
				setStage(EncounterStage.HANDLE_ATTACK);
			}
			break;

		case HANDLE_ATTACK:
			int random = BotFunctions.random(100);
			if (random <= 10) {
				TwitchUser adventurer = getAdventurers().get(BotFunctions.random(getAdventurers().size() - 1));
				String username = adventurer.getUsername();
				questHandler.writeToChat(getCurrentEncounter().getFailedAttackMessage().replaceAll("%username", username));
				deaths.add(adventurer);
				getAdventurers().remove(adventurer);
			} else if (random <= 25) {
				questHandler.writeToChat("The party fails to defeat the enemy.");
				if (BotFunctions.random(3) == 0) {
					setStage(EncounterStage.ATTACKED_MESSAGE);
				} else {
					setStage(EncounterStage.REGROUPING);
				}
			} else {
				questHandler.writeToChat("The party successfully defeated the enemy.");
				setStage(EncounterStage.DEFEATED_ENEMY);
			}
			break;

		case DEFEATED_ENEMY:
			incrimentEncounter();
			setStage(EncounterStage.ENCOUNTER);
			break;
		}

	}

	/**
	 * Sent when the quest is stopped
	 */
	public void onStop() {

		if (getAdventurers().size() > 0) {
			String line = "The party collects their spoils and heads home." + " Survivors get 150% " + questHandler.getChat().getSettings().getPointName() + " on top of their bet. [ ";
			for (TwitchUser point : getAdventurers()) {
				TwitchUser person = getQuestHandler().getChat().getUsers().get(point.getUsername());

				if (person != null) {
					int points = point.getPoints() + (int) (point.getPoints() * 1.5);
					person.addPoints(points);
					line += ("(" + person.getUsername() + ", " + points + "), ");
				}
			}
			line += "]";
			questHandler.writeToChat(line);
		}
		questHandler.setCachedQuestTime(System.currentTimeMillis());
	}

	/**
	 * An array of different spells the person can cast
	 */
	private final String[] SPELLS = new String[] { "Bolt 2", "Fire", "Ice 3", "Comet", "Aqualung", "Matra Magic", "Kjata", "Alexander" };

	/**
	 * Gets a random spell for the person to cast
	 * 
	 * @return The spell a person casts
	 */
	private String getRandomSpell() {
		return SPELLS[BotFunctions.random(SPELLS.length - 1)];
	}

	/**
	 * Performs a few extra attack options
	 */
	private void extraAttackOptions() {
		TwitchUser adventurer = getAdventurers().get(BotFunctions.random(getAdventurers().size() - 1));

		if (adventurer != null) {
			int random = BotFunctions.random(100);

			if (random > 60) {
				questHandler.writeToChat(adventurer.getUsername() + " casts " + getRandomSpell() + ".");
				setStage(EncounterStage.HANDLE_ATTACK);
			} else if (random > 30) {
				questHandler.writeToChat(adventurer.getUsername() + " tried to play the hero and gets killed.");
				getAdventurers().remove(adventurer);
				deaths.add(adventurer);
				setStage(EncounterStage.ATTACKING_MESSAGE);
			} else {
				if (deaths.size() > 0) {
					TwitchUser revived = deaths.get(BotFunctions.random(deaths.size() - 1));
					getAdventurers().add(revived);
					deaths.remove(revived);
					questHandler.writeToChat(adventurer.getUsername() + " revives " + revived.getUsername() + " before the next attack.");
				}
				setStage(EncounterStage.ATTACKING_MESSAGE);
			}
		}
	}

	/**
	 * An abstract method to populate the encounters array
	 */
	public abstract void populateEncounters();

	/**
	 * An abstract method to get the boss encounter
	 * 
	 * @return The boss encounter for this quest
	 */
	public abstract Encounter getBossEncounter();

	public List<TwitchUser> getAdventurers() {
		return adventurers;
	}

	public void setStage(EncounterStage stage) {
		this.stage = stage;
	}

	public String getInitialMessage() {
		return initialMessage;
	}

	/**
	 * Gets the name of the quest
	 * 
	 * @return The name of the quest
	 */
	public String getQuestName() {
		return questName;
	}

	/**
	 * Gets the current stage of the encounter
	 * 
	 * @return
	 */
	public EncounterStage getStage() {
		return stage;
	}

	/**
	 * Gets the current enemy encounter
	 * 
	 * @return
	 */
	public Encounter getCurrentEncounter() {
		return currentEncounter;
	}

	/**
	 * Sets the current enemy encounter
	 * 
	 * @param encounter
	 */
	public void setCurrentEncounter(Encounter encounter) {
		this.currentEncounter = encounter;
	}

	public void setQuestHandler(QuestHandler handler) {
		this.questHandler = handler;
	}

	public QuestHandler getQuestHandler() {
		return questHandler;
	}

}
