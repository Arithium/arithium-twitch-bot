package com.arithium.twitchbot.quest;

/**
 * Different stages to place the party in the encounter
 * 
 * @author Arithium
 *
 */
public enum EncounterStage {

	/**
	 * The party has encountered an enemy
	 */
	ENCOUNTER,

	/**
	 * A message sent when the party is attacking the enemy
	 */
	ATTACKING_MESSAGE,

	/**
	 * A message sent when the enemy is attacking the party
	 */
	ATTACKED_MESSAGE,

	/**
	 * Handles the attack from the enemy to the party
	 */
	HANDLE_ATTACK,

	/**
	 * The party is attacking the enemy
	 */
	ATTACK_ENEMY,

	/**
	 * A message is sent that the party is regrouping to attack the enemy
	 */
	REGROUPING,

	/**
	 * The party has defeated the enemy
	 */
	DEFEATED_ENEMY;

}
