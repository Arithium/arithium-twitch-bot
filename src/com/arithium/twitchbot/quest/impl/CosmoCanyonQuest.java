package com.arithium.twitchbot.quest.impl;
 
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.quest.Encounter;
import com.arithium.twitchbot.quest.Quest;
 
/**
 * A class containing the midgar quest
 * @author Arithium
 *
 */
public class CosmoCanyonQuest extends Quest {
 
	/**
	 * Constructs a new {@link CosmoCanyonQuest} instance
	 */
	public CosmoCanyonQuest() {
		super("Cosmo Canyon", "The expedition in the Cave Of Gi has begun.");
	}
 
	@Override
	public void populateEncounters() {
		try {
			int random = 1 + BotFunctions.random(4);
			int added = 0;
			for (;;) {
 
				Encounter encounter = Encounters.get(BotFunctions.random(Encounters.getEncounters().size() - 1));
				if (encounter == getBossEncounter()) {
					// skip boss encounter
					continue;
				}
 
				addEncounter(encounter);
 
				if (++added == random) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
 
	/**
	 * An enumerator containing all of the available encounters for this quest
	 * 
	 * @author Arithium
	 *
	 */
	public enum Encounters implements Encounter {
 
		STINGER("The party has encountered a Stinger.", "%username gets bitten by the Stinger", 
				"The Stinger poisons the party", "The Stinger shoots a sticky web at the party."),
		
		GI_SPECTERS("The party encounters a group of Gi Specters.", "%username doesn't move out the way quick enough and gets stabbed.", 
				"The Gi Specters cast Death Sentence on the party. Hurry up!", "The Gi Specters swipe at the party with their spears."),
		
		GI_NATTAK("The party has encountered the vengeful spirit Gi Nattak.", "%username trips and gets skewered by Gi Nattak.", 
				"Gi Nattak casts Fire 2 on the party!", "Gi Nattak swipes at the party with his spear.", "Gi Nattak casts Stop on the party."),;
 
		private final String encounter;
		private final String[] attack;
		private final String failedMessage;
		private static List<Encounter> encounters = new ArrayList<>();
 
		Encounters(String encounter, String failedMessage, String... attack) {
			this.encounter = encounter;
			this.attack = attack;
			this.failedMessage = failedMessage;
		}
 
		static {
			encounters.addAll(EnumSet.allOf(Encounters.class));
		}
 
		public static Encounter get(int index) {
			return encounters.get(index);
		}
 
		public static List<Encounter> getEncounters() {
			return encounters;
		}
 
		@Override
		public String encounterMessage() {
			return encounter;
		}
 
		@Override
		public String onAttackMessage() {
			return attack[BotFunctions.random(attack.length - 1)];
		}
 
		@Override
		public String getFailedAttackMessage() {
			return failedMessage;
		}
 
		@Override
		public String toString() {
			return name().toLowerCase().replace("_", " ");
		}
 
	}
 
	@Override
	public Encounter getBossEncounter() {
		return Encounters.GI_NATTAK;
	}
 
}