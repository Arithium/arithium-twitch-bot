package com.arithium.twitchbot.quest.impl;
 
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
 
import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.quest.Encounter;
import com.arithium.twitchbot.quest.Quest;
 
/**
 * A class containing the midgar quest
 * @author Arithium
 *
 */
public class UnderwaterReactorQuest extends Quest {
 
	/**
	 * Constructs a new {@link UnderwaterReactorQuest} instance
	 */
	public UnderwaterReactorQuest() {
		super("Junon ", "The raid on the Underwater Reactor has begun.");
	}
 
	@Override
	public void populateEncounters() {
		try {
			int random = 1 + BotFunctions.random(4);
 
			if (random < 1) {
				random = 1;
			}
 
			int added = 0;
			for (;;) {
 
				Encounter encounter = Encounters.get(BotFunctions.random(Encounters.getEncounters().size() - 1));
				if (encounter == getBossEncounter()) {
					// skip boss encounter
					continue;
				}
 
				addEncounter(encounter);
 
				if (++added == random) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
 
	/**
	 * An enumerator containing all of the available encounters for this quest
	 * 
	 * @author Arithium
	 *
	 */
	public enum Encounters implements Encounter {
 
		SLALOM_DEATHMACHINE("The party has encountered a group of Slalom and Death Machine.", "%username gets Punched to death by Slalom", 
				"Death Machine uses Smoke Screen and causes %username to go Blind", "Death Machine uses Matra Magic on the party."),
		
		SUBMARINE_CREW("The party encounters a group of Shinra Submarine Soldiers.", "%username forgot to heal and gets Shot to death.", 
				"Shinra Soldier A throws a Grenade on the party.", "Shinra Soldier B throws a Grenade on the party and %username caught the grenade ..... BOOOOOM!.", "Shinra Soldier C throws a Grenade on the party and %username caught the grenade and throws it back .....*ArithiumBot: lucky bastard*."),
		
		SOLDIER_2ND("The party encounters a group of SOLDIER 2ND Class.", "%username couldn't dodge and was Hacked in half by SOLDIER 2ND A .... *ArithiumBot: ... how gory*.", 
				"SOLDIER 2ND B throws his Sickle Slicer.", "SOLDIER 2ND A uses Sword of Doom."),
		
		MIMIC("The party found a Treasure Chest, unfortunately it was a Mimic..", "%username got eaten while trying to open the Chest .... *ArithiumBot *Nilsen Face*: HA HA *.", 
				"Mimic uses Lick on the party and they're Slowed and Saddened", "Mimic uses Bite."),
		
		GHOST_SHIP("The party encounters a Ghost Ship.", "Ghost Ship uses Goannai %username gets stuck to the paddle and get thrown away.... *ArithiumBot: BYEEeeee~~~*.", 
				"Ghost Ship used Whack", "Ghost Ship uses St. Elmo's Fire."),
		
		EMERALD_WEAPON("The party has encountered the Carry Armo---- something crashed through the Reactor Walls ...IT'S EMERALD WEAPON.", "%username gets scared the HONK to death.", 
				"EMERALD uses Emerald Beam on the party!", "EMERALD uses Revenge Stomp on the party.", "EMERALD readies Aire Tam Storm." , "EMERALD uses Aire Tam Storm on the party ... the party doesn't have any Materia they take No Damage ... *ArithiumBot: Sucks to be you Emerald kukukuku*.", "EMERALD hears ArithiumBot in the background and uses Emerald Shoot .... ArithiumBot Dies."),;
 
		private final String encounter;
		private final String[] attack;
		private final String failedMessage;
		private static List<Encounter> encounters = new ArrayList<>();
 
		Encounters(String encounter, String failedMessage, String... attack) {
			this.encounter = encounter;
			this.attack = attack;
			this.failedMessage = failedMessage;
		}
 
		static {
			encounters.addAll(EnumSet.allOf(Encounters.class));
		}
 
		public static Encounter get(int index) {
			return encounters.get(index);
		}
 
		public static List<Encounter> getEncounters() {
			return encounters;
		}
 
		@Override
		public String encounterMessage() {
			return encounter;
		}
 
		@Override
		public String onAttackMessage() {
			return attack[BotFunctions.random(attack.length - 1)];
		}
 
		@Override
		public String getFailedAttackMessage() {
			return failedMessage;
		}
 
		@Override
		public String toString() {
			return name().toLowerCase().replace("_", " ");
		}
 
	}
 
	@Override
	public Encounter getBossEncounter() {
		return Encounters.EMERALD_WEAPON;
	}
 
}