package com.arithium.twitchbot.quest.impl;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.quest.Encounter;
import com.arithium.twitchbot.quest.Quest;

/**
 * A class containing the midgar quest
 * @author Arithium
 *
 */
public class MidgarQuest extends Quest {

	/**
	 * Constructs a new {@link MidgarQuest} instance
	 */
	public MidgarQuest() {
		super("Midgar", "The quest to Midgar has begun.");
	}

	@Override
	public void populateEncounters() {
		try {
			int random = 1 + BotFunctions.random(4);
			int added = 0;
			
			for (;;) {

				Encounter encounter = Encounters.get(BotFunctions.random(Encounters.getEncounters().size() - 1));
				if (encounter == getBossEncounter()) {
					// skip boss encounter
					continue;
				}

				addEncounter(encounter);

				if (++added == random) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * An enumerator containing all of the available encounters for this quest
	 * 
	 * @author Arithium
	 *
	 */
	public enum Encounters implements Encounter {

		TURRET("The party has encountered some turrets.", "%username gets shot by the turret", 
				"The turrets fire machine guns at the party", "The turrets shoot some missiles at the party."),
		
		SOLDIERS("The party encounters a group of soldiers.", "%username doesn't move out the way quick enough and gets shot.", 
				"The soldiers fire shots at the party.", "The soldiers throw a grenade at the party."),
		
		GUARD_SCORPION("The party has found the Guard Scorpion.", "%Trips and gets crushed by Guard Scorpion", 
				"The Guard scorpion attacks with his tail laser.", "Guard Scorpion fires at the party.", "Guard Scorpion attacks with his razer sharp tail."),;

		private final String encounter;
		private final String[] attack;
		private final String failedMessage;
		private static List<Encounter> encounters = new ArrayList<>();

		Encounters(String encounter, String failedMessage, String... attack) {
			this.encounter = encounter;
			this.attack = attack;
			this.failedMessage = failedMessage;
		}

		static {
			encounters.addAll(EnumSet.allOf(Encounters.class));
		}

		public static Encounter get(int index) {
			return encounters.get(index);
		}

		public static List<Encounter> getEncounters() {
			return encounters;
		}

		@Override
		public String encounterMessage() {
			return encounter;
		}

		@Override
		public String onAttackMessage() {
			return attack[BotFunctions.random(attack.length - 1)];
		}

		@Override
		public String getFailedAttackMessage() {
			return failedMessage;
		}
		
		@Override
		public String toString() {
			return name().toLowerCase().replace("_", " ");
		}

	}

	@Override
	public Encounter getBossEncounter() {
		return Encounters.GUARD_SCORPION;
	}

}
