package com.arithium.twitchbot.quest.impl;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.quest.Encounter;
import com.arithium.twitchbot.quest.Quest;

/**
 * A quest to mount nibel
 * 
 * @author Arithium
 *
 */
public class MountNibelQuest extends Quest {

	public MountNibelQuest() {
		super("Mount Nibel", "The adventurers begin their journey to the heart of Mount Nibel.");
	}

	/**
	 * An enumerator containing all of the available encounters for this quest
	 * 
	 * @author Arithium
	 *
	 */
	public enum Encounters implements Encounter {

		KYUVILDENS("The party has encountered a group of Kyuvildens.", "%username slips and gets stung by the Kyuvilden's stinger.", 
				"The Kyuvildens sting the party."),

		SCREAMER(
				"The party has encountered a group of Screamers.",
				"%username falls off the side of the cliff and perishes.",
				"Screamer Screeches loudly and disrupts the party's focus.",
				"Screamer attacks the party."),

		SONIC_SPEED(
				"The party has encountered a group of Sonic Speeds.",
				"%username looked the wrong way and was attacked from behind.",
				"The Sonic Speed uses harrier on the party.",
				"The Sonic Speed swoops down at the party."),

		ZUU(
				"The party has encountered a mighty Zuu.",
				"%username tripped over a rock and gets blown away by a whirlwind.",
				"Zuu blows a whirlwind at the party.",
				"Zuu attacks the party with its mighty claws."),

		GREEN_DRAGON(
				"The party encounters a Green Dragon.",
				"%username doesn't move out the way quick enough and gets torched by the dragons flame.",
				"The Dragon claws at the party.",
				"The Dragon breathes fire on the party."),

		MATERIA_KEEPER(
				"The party has stumbled across the fierce Materia Keeper.",
				"%username trips and gets impaled by the Materia Keeper.",
				"Materia Keeper casts trine on the party!",
				"Materia Keeper slices the party with its sharp legs."),;

		private final String encounter;
		private final String[] attack;
		private final String failedMessage;
		private static List<Encounter> encounters = new ArrayList<>();

		Encounters(String encounter, String failedMessage, String... attack) {
			this.encounter = encounter;
			this.attack = attack;
			this.failedMessage = failedMessage;
		}

		static {
			encounters.addAll(EnumSet.allOf(Encounters.class));
		}

		public static Encounter get(int index) {
			return encounters.get(index);
		}

		public static List<Encounter> getEncounters() {
			return encounters;
		}

		@Override
		public String encounterMessage() {
			return encounter;
		}

		@Override
		public String onAttackMessage() {
			return attack[BotFunctions.random(attack.length - 1)];
		}

		@Override
		public String getFailedAttackMessage() {
			return failedMessage;
		}

		@Override
		public String toString() {
			return name().toLowerCase().replace("_", " ");
		}

	}

	@Override
	public Encounter getBossEncounter() {
		return Encounters.MATERIA_KEEPER;
	}

	@Override
	public void populateEncounters() {
		try {
			int random = 1 + BotFunctions.random(4);
			int added = 0;
			for (;;) {

				Encounter encounter = Encounters.get(BotFunctions.random(Encounters.getEncounters().size() - 1));
				if (encounter == getBossEncounter()) {
					// skip boss encounter
					continue;
				}

				addEncounter(encounter);
				added++;
				
				if (added == random) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
