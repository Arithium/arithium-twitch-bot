package com.arithium.twitchbot.quest;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.arithium.twitchbot.BotBootstrap;
import com.arithium.twitchbot.BotFunctions;
import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.point.TwitchUser;
import com.arithium.twitchbot.quest.impl.CosmoCanyonQuest;
import com.arithium.twitchbot.quest.impl.MidgarQuest;
import com.arithium.twitchbot.quest.impl.MountNibelQuest;
import com.arithium.twitchbot.quest.impl.UnderwaterReactorQuest;

/**
 * Handles processing the quests
 * 
 * @author Arithium
 *
 */
public class QuestHandler {

	/**
	 * The chat instance for this quest handler
	 */
	private TwitchChat chat;

	/**
	 * A list of quests
	 */
	private static List<Quest> quests = new ArrayList<>();

	/**
	 * Our currently active quest
	 */
	private Quest quest;

	/**
	 * The delay from our last quest to prevent flooding
	 */
	private long lastQuestTime;

	/**
	 * The delay between each queue attempt
	 */
	private int queueDelay = 0;

	/**
	 * Returns if the quest is currently active
	 */
	private boolean questActive;

	/**
	 * A queue to store pending usernames into the quest
	 */
	private Queue<TwitchUser> queue = new ConcurrentLinkedQueue<>();

	static {
		quests.add(new MidgarQuest());
		quests.add(new CosmoCanyonQuest());
		quests.add(new MountNibelQuest());
		quests.add(new UnderwaterReactorQuest());
	}

	/**
	 * Constructs a new {@link QuestHandler} for the provided {@link TwitchChat}
	 * 
	 * @param chat
	 *            The chat to handle quests for
	 */
	public QuestHandler(TwitchChat chat) {
		this.chat = chat;
	}

	/**
	 * Queue a username to be added to the quest
	 * 
	 * @param username
	 *            The username to add to the quest
	 */
	public void queueUser(TwitchUser point) {

		for (TwitchUser q : queue) {
			if (q.getUsername().equalsIgnoreCase(point.getUsername())) {
				return;
			}
		}

		if (quest != null) {
			for (TwitchUser q : quest.getAdventurers()) {
				if (q.getUsername().equalsIgnoreCase(point.getUsername())) {
					return;
				}
			}
		}

		// System.out.println("Added to queue: " + username);
		TwitchUser person = chat.getUsers().get(point.getUsername());

		if (person != null) {
			person.removePoints(point.getPoints());
		}
		
		chat.sendMessage(chat.getChannels()[0], "/w " + person.getUsername() + " You have been added to the quest with a bet of " + point.getPoints() + " " + BotBootstrap.getBoostrap().getSettings().getPointName() + ".");
		queue.add(point);
	}

	/**
	 * Adds a user to the quest
	 * 
	 * @param username
	 *            The username to add to the quest
	 */
	public void addUser(String username, int bet) {
		if (quest == null || quest.getAdventurers().contains(username)) {
			return;
		}
		quest.getAdventurers().add(new TwitchUser(username, bet));
	}

	/**
	 * This handles the cycling for quests
	 */
	public void questCycle() {

		if (!getActiveQuest()) {

			if (queue.size() < 1 && quest == null) {
				queueDelay = 0;
				return;
			}

			while (quest == null) {
				Class<?> clazz = quests.get(BotFunctions.random(quests.size() - 1)).getClass();
				try {
					quest = (Quest) clazz.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
					quest = new MidgarQuest();
				}
				
				quest.resetQuest();
				quest.setQuestHandler(this);
				int survivalChance = 70;
				int userBoost = 2;
				survivalChance += userBoost;
				writeToChat("A quest to " + quest.getQuestName() + " has started. There is a " + survivalChance + "% chance to survive. Type !quest <bet> to join now!");
			}

			queueDelay++;

			if (queueDelay % 15 == 0 && queue.size() > 0) {
				List<String> added = new ArrayList<>();
				TwitchUser point = null;
				while ((point = queue.poll()) != null) {
					addUser(point.getUsername(), point.getPoints());
					added.add(point.getUsername());
				}

				if (added.size() > 0)
					writeToChat(added + " has been added to the quest.");
			} else if (queueDelay == 60) {
				int survivalChance = 70;
				int userBoost = (quest.getAdventurers().size() * 2);
				if (userBoost > 20) {
					userBoost = 20;
				}
				survivalChance += userBoost;
				writeToChat("1 minute before the quest begins. There is a " + survivalChance + "% chance to survive. Type !quest <bet> to join now.");
			} else if (queueDelay == 90) {
				int survivalChance = 70;
				int userBoost = (quest.getAdventurers().size() * 2);
				if (userBoost > 20) {
					userBoost = 20;
				}
				survivalChance += userBoost;
				writeToChat("30 seconds before the quest begins. There is a " + survivalChance + "% chance to survive. Type !quest <bet> to join now.");
			} else if (queueDelay >= 120) {

				if (quest.getAdventurers().size() > 0) {
					writeToChat(quest.getInitialMessage());
					questActive = true;
				}
			}
		} else {

			if (!quest.tick()) {
				quest.onStop();
				queueDelay = 0;
				quest = null;
				questActive = false;
			}
		}
		// System.out.println(queueDelay + " : " + queue + " : " + quest);
	}

	/**
	 * Gets the cached time from the last quest
	 * 
	 * @return The cached time from the last quest
	 */
	public Long cachedTime() {
		return lastQuestTime;
	}
	
	public void setCachedQuestTime(long time) {
		this.lastQuestTime = time;
	}

	/**
	 * Returns if there is an active quest going on
	 * 
	 * @return
	 */
	public boolean getActiveQuest() {
		return questActive;
	}

	/**
	 * Writes a line to the chat
	 * 
	 * @param message
	 */
	public void writeToChat(String message) {
		chat.sendMessage(chat.getChannels()[0], "[Quest Bot] " + message);

	}

	/**
	 * Gets the {@link TwitchChat} for this quest handler
	 * 
	 * @return The chat associated with this quest handler
	 */
	public TwitchChat getChat() {
		return chat;
	}

}
