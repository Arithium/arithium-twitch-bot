package com.arithium.twitchbot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.frame.ClientFrame;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * A container class to store the channels connected to the bot
 * 
 * @author Arithium
 *
 */
public class ChannelContainer {

	/**
	 * A map to store all of the channels in for referencing
	 */
	private static Map<String, TwitchChat> channels = new HashMap<>();

	/**
	 * Adds a channel to the map for referencing
	 * 
	 * @param channel
	 *            The name of the channel to store
	 * @param chat
	 *            The {@link TwitchChat} associated with that channel
	 */
	public static void addChannel(String channel, TwitchChat chat) {
		if (!channels.containsKey(channel)) {
			channels.put(channel, chat);
		}
	}

	/**
	 * Removes a channel from the container since its no longer connected to our
	 * chat
	 * 
	 * @param channel
	 *            The name of the channel to remove
	 */
	public static void removeChannel(String channel) {
		if (channels.containsKey(channel)) {
			channels.remove(channel);
		}
	}

	/**
	 * Saves the channel container so we can re-connect after a restart
	 */
	public static void save() {
		Path path = Paths.get("./data/");
		
		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		Path file = Paths.get(path.toString() + "/channels.json");

		if (!Files.exists(file)) {
			try {
				Files.createFile(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		List<String> c = new ArrayList<>();

		for (Entry<String, TwitchChat> entry : channels.entrySet()) {
			if (!entry.getKey().equalsIgnoreCase(BotBootstrap.getBoostrap().getSettings().getUsername())) {
				c.add(entry.getKey());
			}
		}
		

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try {
			BufferedWriter writer = Files.newBufferedWriter(file);
			String json = gson.toJson(c, List.class);
			writer.write(json);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads all of the saved channels and attempts to re-connect to them
	 */
	public static void load() {
		Path path = Paths.get("./data/");

		if (!Files.exists(path)) {
			return;
		}

		Path file = Paths.get(path.toString() + "/channels.json");

		if (!Files.exists(file)) {
			return;
		}

		List<String> channelList = null;

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try {
			Type type = new TypeToken<List<String>>() {
			}.getType();
			BufferedReader reader = Files.newBufferedReader(file);
			channelList = (gson.fromJson(reader, type));

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (channelList == null) {
			return;
		}

		Executor executor = Executors.newCachedThreadPool();

		final List<String> a = Collections.unmodifiableList(channelList);

		executor.execute(() -> {
			for (String s : a) {
				if (!s.equalsIgnoreCase(BotBootstrap.getBoostrap().getSettings().getUsername())) {
					TwitchChat chat = BotBootstrap.getBoostrap().start(BotBootstrap.getBoostrap().getSettings().getUsername(), BotBootstrap.getBoostrap().getSettings().getOAuth(), s);
					if (chat != null) {
						channels.put(s, chat);
						ClientFrame.addToList(s);
					}
				}
			}
		});

	}

	/**
	 * Gets our map of channels currently connected to the bot
	 * 
	 * @return The map filled with connected channels
	 */
	public static Map<String, TwitchChat> getChannels() {
		return channels;
	}

}
