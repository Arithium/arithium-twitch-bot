package com.arithium.twitchbot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.JSONException;
import org.json.JSONObject;

import com.arithium.twitchbot.chat.TwitchChat;
import com.arithium.twitchbot.chat.TwitchFollowObject;
import com.arithium.twitchbot.chat.TwitchGroup;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * A class of functions for the bot
 * 
 * @author Tim
 *
 */
public class BotFunctions {

	/**
	 * Fetches the latest group of chatters in the chat
	 * 
	 * @return The latest group of chatters in the chat
	 * @throws IOException
	 * @throws JSONException
	 */
	static TwitchGroup fetchLatestGroup(TwitchChat chat) {
		try {
			JSONObject jsonObject = readJsonFromUrl("https://tmi.twitch.tv/group/user/" + chat.getChannels()[0].replaceAll("#", "") + "/chatters");
			Gson gson = new Gson();
			return gson.fromJson(jsonObject.toString(), TwitchGroup.class);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Reads the date the person followed the channel
	 * 
	 * @param username
	 *            The username of the person to check who followed
	 * @return The date the person followed
	 * @throws IOException
	 * @throws JSONException
	 */
	public static TwitchFollowObject readFollow(TwitchChat chat, String username) throws IOException, JSONException {
		JSONObject jsonObject = readJsonFromUrl("https://api.twitch.tv/kraken/users/" + username + "/follows/channels/" + chat.getChannels()[0].replaceAll("#", "") + "?client_id="
				+ BotBootstrap.getBoostrap().getSettings().getClientId() + "");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		TwitchFollowObject o = gson.fromJson(jsonObject.toString(), TwitchFollowObject.class);

		return o;
	}

	/**
	 * Reads all of the data from a {@link Reader}
	 * 
	 * @param rd
	 *            The {@link Reader} reading from an input source
	 * @return The data read from the reader
	 * @throws IOException
	 */
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	/**
	 * Gets a {@link JSONObject} object from the url
	 * 
	 * @param url
	 *            The url to get the json object from
	 * @return The {@link JSONObject} from the url
	 * @throws IOException
	 * @throws JSONException
	 */
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

	/**
	 * Saves the bot settings in Json format
	 * 
	 * @param settings
	 *            The {@link BotSettings} to save
	 */
	public static void saveBotSettings(BotSettings settings) {

		Path path = Paths.get("./data/");

		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Path file = Paths.get(path.toString() + "/settings.json");

		BotSettings existing = BotBootstrap.getBoostrap().getSettings();

		if (!BotBootstrap.getBoostrap().isRememberSettings()) {
			existing = new BotSettings("", "", "", "points");
		}

		BufferedWriter writer = null;
		try {
			writer = Files.newBufferedWriter(file);
			writer.write(gson.toJson(existing, BotSettings.class));
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads the bot settings from the Json file
	 */
	public static void readBotSettings() {

		Path path = Paths.get("./data/");

		if (!Files.exists(path)) {
			return;
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Path file = Paths.get(path.toString() + "/settings.json");

		if (!Files.exists(file)) {
			return;
		}

		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(file);
			BotSettings settings = gson.fromJson(reader, BotSettings.class);
			BotBootstrap.getBoostrap().setBotSettings(settings);

			if (!settings.getUsername().isEmpty()) {
				BotBootstrap.getBoostrap().setRememberSettings(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Returns a random value from 0 - range
	 * 
	 * @param range
	 *            The range to get a random value from
	 * @return A random number from 0 - range
	 */
	public static int random(int range) {
		return (int) (Math.random() * (range + 1));
	}

}
