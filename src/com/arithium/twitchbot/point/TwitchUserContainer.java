package com.arithium.twitchbot.point;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.arithium.twitchbot.BotBootstrap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

/**
 * A container to store the user data for a specific chat
 * 
 * @author Arithium
 *
 */
public class TwitchUserContainer {

	/**
	 * A map to store the users in
	 */
	private Map<String, TwitchUser> users;

	/**
	 * Constructs a new {@link TwitchUserContainer}
	 */
	public TwitchUserContainer() {
	}

	/**
	 * Saves the user data for the channel
	 * 
	 * @param channel
	 *            The name of the channel
	 */
	public void save(String channel) {
		Path path = Paths.get("./data" + "/channels/" + channel.replaceAll("#", ""));

		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String[] toRemove = { "nightbot", "moobot", BotBootstrap.getBoostrap().getSettings().getUsername().toLowerCase() };

		for (String s : toRemove)
			users.remove(s);

		Map<String, TwitchUser> collection = Collections.unmodifiableMap(users);
		try {
			Type type = new TypeToken<Map<String, TwitchUser>>() {
			}.getType();
			BufferedWriter writer = Files.newBufferedWriter(Paths.get(path.toFile().getPath() + "/user_points.json"));
			writer.write(gson.toJson(collection, type));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads the user data for the channel
	 * 
	 * @param channel
	 *            The name of the channel
	 */
	public void load(String channel) {
		Path path = Paths.get("./data" + "/channels/" + channel.replaceAll("#", ""));

		if (!Files.exists(path)) {
			setMap(new HashMap<String, TwitchUser>());
			return;
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Path file = Paths.get(path.toFile().getPath() + "/user_points.json");

		if (!Files.exists(file)) {
			return;
		}
		try {
			Type type = new TypeToken<Map<String, TwitchUser>>() {
			}.getType();
			JsonReader reader = new JsonReader(new StringReader(file.toFile().getPath()));
			reader.setLenient(true);
			Map<String, TwitchUser> map = new HashMap<>();
			try {
				map = (gson.fromJson(reader, type));
			} catch (Exception e) {
				System.out.println("Failed to load channel " + channel + ".");
//				e.printStackTrace();
			}
			setMap(map);
			reader.close();
		} catch (Exception e) {
			System.out.println("Failed to load channel " + channel + ".");
			e.printStackTrace();
			setMap(new HashMap<String, TwitchUser>());
		}
	}

	/**
	 * Gets all of the {@link TwitchUser}'s in the channel
	 * 
	 * @return The users in the channel
	 */
	public Map<String, TwitchUser> getUsers() {
		return users;
	}

	/**
	 * Sets the map of users in the channel
	 * 
	 * @param map
	 *            A map containing users
	 */
	public void setMap(Map<String, TwitchUser> map) {
		users = map;
	}

	/**
	 * Gets the data for a user from the channel
	 * 
	 * @param username
	 *            The username of the user to get
	 * @return The data for the user
	 */
	public TwitchUser get(String username) {
		if (getUsers() == null) {
			return null;
		}
		TwitchUser user = getUsers().get(username.toLowerCase());

		if (user == null) {
			add(username);
		}
		return getUsers().get(username.toLowerCase());
	}

	/**
	 * Adds a new user to the channel
	 * 
	 * @param username
	 *            The username of the user
	 */
	public void add(String username) {
		users.put(username, new TwitchUser(username, 5));

	}

}
