package com.arithium.twitchbot.point;

/**
 * Represents a single twitch user
 * 
 * @author Arithium
 *
 */
public class TwitchUser {

	/**
	 * The username of the user
	 */
	private String username;

	/**
	 * The amount of points the user has
	 */
	private int points;

	/**
	 * The amount of minutes the user has been active in the chat
	 */
	private int minutes;

	/**
	 * Constructs a new {@link TwitchUser} for the chat
	 * 
	 * @param username
	 *            The username of the user
	 * @param points
	 *            The amount of points to start the user off with
	 */
	public TwitchUser(String username, int points) {
		this.username = username;
		this.points = points;
		this.minutes = 0;
	}

	/**
	 * Adds points to a users total points
	 * 
	 * @param points
	 *            The amount of points to add to the user
	 */
	public void addPoints(int points) {
		this.points += points;
	}

	/**
	 * Removes points from a user
	 * 
	 * @param points
	 *            The amount of points to remove from the user
	 */
	public void removePoints(int points) {
		if (this.points - points < 0) {
			points = this.points - points;
		}
		this.points -= points;
	}

	/**
	 * Gets the username of the user
	 * 
	 * @return The username of the user
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Gets the amount of points the user has
	 * 
	 * @return The amoutn of points the user has
	 */
	public int getPoints() {
		return points;
	}

	/**
	 * Sets the amount of minutes the user has been in the chat
	 * 
	 * @param minutes
	 *            The amount of minutes the user has been in the chat
	 */
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	/**
	 * Gets the amount of minutes the user has been in the chat
	 * 
	 * @return The amount of minutes the user has been in the chat
	 */
	public int getMinutes() {
		return minutes;
	}

}
