package com.arithium.twitchbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class LineCounter {

	private static int lines = 0;

	public static void main(String[] args) throws IOException {
		System.out.println("Starting the line count for the project.");
		Path startPath = Paths.get("./src/");
		Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				String firstLine = "";

				BufferedReader reader = Files.newBufferedReader(file, Charset.defaultCharset());
				while ((firstLine = reader.readLine()) != null) {
					lines++;
				}
				return FileVisitResult.CONTINUE;
			}
		});
		System.out.println("Finished scanning the java files.");
		System.out.println("Total lines in project: " + lines);
	}

}
